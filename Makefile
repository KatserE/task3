OUTPUT	:= "./output/"
SRC		:= "task3.swift"
NAME	:= "task3"

all:
	@test -d $(OUTPUT) || mkdir $(OUTPUT)
	@swiftc -O $(SRC) -o $(OUTPUT)/$(NAME)
	@echo Compilation is successful!

clean:
	@test -d $(OUTPUT) && rm -rf $(OUTPUT)
