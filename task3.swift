// MARK: - Sort array
var arr = Array(0..<20).shuffled()

arr.sort(by: <);
print("Direct order:\n\(arr)")

arr.sort(by: >);
print("Reversed order:\n\(arr)\n")


// MARK: - Sort friends' names
// MARK: Function that gets friends' names from user
func getFriendsNames() -> [String] {
    var allFriends = [String]()
    var curName: String

    while true {
        print("Enter a friend name (to leave enter empty line) -- ", terminator: "")
        curName = readLine()!
        if curName.isEmpty { break }

        allFriends.append(curName)
    }

    return allFriends
}

// MARK: Sort names by number of letters
var friends = getFriendsNames()
friends.sort { $0.count < $1.count }
print("\nSorted by number of letters:\n\(friends)")

// MARK: Create dictionary from name's length and it
let friendsInDict = Dictionary(friends.map { ($0.count, $0) }) {
    (first, _) in first
}

// MARK: Write function that gets key of the dict and prints key and value by this key
// It's strange to write a function in the part of closures, soooo...
print("\nDictionary with friends. Key -- number of letters, value -- friend name:")
friendsInDict.keys.forEach {
    print("\($0) -- \(friendsInDict[$0]!)")
}
print() // To separate the output


// MARK: - Write function that gets two arrays and checks them for emptiness.
// If an array is empty, add a value and print it to console

// And again it's strange to write a function in the part of closures soooo...
let checker = { (arrStr: inout [String], arrNum: inout [Int]) in
    if arrStr.isEmpty {
        arrStr.append(String(Int.random(in: 0...100)))
    }
    if arrNum.isEmpty {
        arrNum.append(Int.random(in: 0...100))
    }

    print(arrStr)
    print(arrNum)
}

// MARK: Empty string array, empty number array
var arrStr = [String]()
var arrNum = [Int]()

print("Empty string array, empty number array:")
checker(&arrStr, &arrNum)
print()

// MARK: Empty string array, not empty number array
arrStr = []
arrNum = [1, 2, 3, 4, 5]

print("Empty string array, not empty number array:")
checker(&arrStr, &arrNum)
print()

// MARK: Not empty string array, empty number array
arrStr = ["One", "Two", "Three"]
arrNum = []

print("Not empty string array, empty number array:")
checker(&arrStr, &arrNum)
print()

// MARK: Not empty string array, not empty number array
print("Not empty string array, not empty number array:")
checker(&arrStr, &arrNum)
